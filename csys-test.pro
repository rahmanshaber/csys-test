TEMPLATE = app
TARGET = csys-test

QT += dbus
INCLUDEPATH += .

SOURCES += csys-test.cpp storageinfo.cpp
HEADERS += storageinfo.h

# Disable warnings, enable threading support and c++11
CONFIG  += thread silent build_all c++11

# Definetion section
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += "HAVE_POSIX_OPENPT"

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR


        INSTALLS += target 
}

